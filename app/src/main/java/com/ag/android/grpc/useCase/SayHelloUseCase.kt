package com.ag.android.grpc.useCase

import com.ag.android.grpc.model.UseCase
import com.ag.android.grpc.repository.GreeterRepository

class SayHelloUseCase constructor(
    private val greeterRepository: GreeterRepository
): UseCase<String, String>() {

    override suspend fun execute(parameters: String): String {

        return greeterRepository.sayHello(parameters)
    }

}