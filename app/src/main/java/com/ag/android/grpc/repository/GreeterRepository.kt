package com.ag.android.grpc.repository

import com.ag.android.grpc.remoteDataSource.grpc.GreeterRemoteDataSource

class GreeterRepository constructor(private val greeterRemoteDataSource: GreeterRemoteDataSource) {

    suspend fun sayHello(message: String): String {

        return greeterRemoteDataSource.sayHello(message)
    }
}