package com.ag.android.grpc.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.ag.android.grpc.useCase.SayHelloUseCase
import com.ag.android.grpc.model.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class GreeterViewModel constructor(private val sayHelloUseCase: SayHelloUseCase) : ViewModel() {

    private var sayHelloJob: Job? = null

    fun sayHello(message: String): LiveData<Result<String>> {

        val liveDataMerger = MediatorLiveData<Result<String>>()

        sayHelloJob?.cancel()

        sayHelloJob = GlobalScope.launch(Dispatchers.Main) {

            liveDataMerger.addSource(sayHelloUseCase.invoke(message)) { value ->
                liveDataMerger.setValue(value)
            }

        }

        return liveDataMerger
    }

    override fun onCleared() {
        super.onCleared()
        sayHelloJob?.cancel()
    }

}