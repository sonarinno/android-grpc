package com.ag.android.grpc.di

import com.ag.android.grpc.remoteDataSource.grpc.GreeterRemoteDataSource
import com.ag.android.grpc.remoteDataSource.grpc.GrpcService
import com.ag.android.grpc.repository.GreeterRepository
import com.ag.android.grpc.useCase.SayHelloUseCase
import com.ag.android.grpc.viewModel.GreeterViewModelFactory

object Injector {

    // Provide GrpcService
    fun provideGrpcService() = GrpcService(
        "35.240.235.132",
        50051
    )

    // Provide DataSource
    fun provideGreeterRemoteDataSource(grpcService: GrpcService) = GreeterRemoteDataSource(grpcService)

    // Provide Repository
    fun provideGreeterRepository(greeterRemoteDataSource: GreeterRemoteDataSource) = GreeterRepository(
        greeterRemoteDataSource
    )

    // Provide UseCase
    fun provideSayHelloUseCase(greeterRepository: GreeterRepository) = SayHelloUseCase(
        greeterRepository
    )

    // Provide ViewModelFactory
    fun provideGreeterViewModelFactory() = GreeterViewModelFactory(
        provideSayHelloUseCase(
            provideGreeterRepository(
                provideGreeterRemoteDataSource(
                    provideGrpcService()
                )
            )
        )
    )
}