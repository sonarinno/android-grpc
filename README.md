# Android-GRPC

* เตรียมไฟล์ GRPC สำหรับ Android และ iOS
* เตรียม GRPC Library ให้กับ Android Project
* การ Build protobuf ด้วย Android Project
* การใช้งาน Class หลังการ Build protobuf
* ทดสอบการทำงาน

## เตรียมไฟล์ GRPC สำหรับ Android และ iOS
สร้างไฟล์ helloworld.proto ดังนี้
```protobuf
syntax = "proto3";

// The java definition
option java_multiple_files = true;
option java_package = "io.grpc.examples.helloworld";
option java_outer_classname = "HelloWorldProto";

// The objc definition
option objc_class_prefix = "HLW";

package helloworld;

// The greeting service definition.
service Greeter {
    // Sends a greeting
    rpc SayHello(HelloRequest) returns (HelloResponse) {}
    
    // Sends another greeting
    rpc SayHelloAgain (HelloRequest) returns (HelloResponse) {}
}

// The request message containing the user's name
message HelloRequest {
    string name = 1; 
}

// The response message cotaining the gretting
message HelloResponse {
    string message = 1;
}
```
กำหนดค่าเพื่อให้สามารถใช้กับภาษา Java ได้
```protobuf
// The java definition
option java_multiple_files = true;
option java_package = "io.grpc.examples.helloworld";
option java_outer_classname = "HelloWorldProto";
```
กำหนดค่าเพื่อให้สามารถใช้กับภาษา Objective C ได้
```protobuf
// The objc definition
option objc_class_prefix = "HLW";
```
## เตรียม GRPC Library ให้กับ Android Project

เพิ่มการใช้งาน Androidx เข้าไปใน gradle.properties

```properties
# Project-wide Gradle settings.
# IDE (e.g. Android Studio) users:
# Gradle settings configured through the IDE *will override*
# any settings specified in this file.
# For more details on how to configure your build environment visit
# http://www.gradle.org/docs/current/userguide/build_environment.html
# Specifies the JVM arguments used for the daemon process.
# The setting is particularly useful for tweaking memory settings.
org.gradle.jvmargs=-Xmx1536m
# When configured, Gradle will run in incubating parallel mode.
# This option should only be used with decoupled projects. More details, visit
# http://www.gradle.org/docs/current/userguide/multi_project_builds.html#sec:decoupled_projects
# org.gradle.parallel=true
# Kotlin code style for this project: "official" or "obsolete":
kotlin.code.style=official

android.useAndroidX=true
android.enableJetifier=true

```

เพิ่ม protobuf-gradle-plugin เข้าไปใน build.gradle ของโปรเจค
```gradle
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    ext.kotlin_version = '1.3.31'
    repositories {
        google()
        jcenter()

        // Add for grpc
        mavenCentral()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.4.2'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"

        // Add for grpc
        classpath 'com.google.protobuf:protobuf-gradle-plugin:0.8.10'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()

        // Add for grpc
        mavenCentral()
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}
```
เรียกใช้ protobuf plugin และ library ใน app/build.gradle
```gradle
apply plugin: 'com.android.application'

apply plugin: 'kotlin-android'

apply plugin: 'kotlin-android-extensions'

// Add for grpc
apply plugin: 'com.google.protobuf'

android {
    compileSdkVersion 28
    defaultConfig {
        applicationId "com.ag.android.grpc"
        minSdkVersion 19
//        minSdkVersion 28
        targetSdkVersion 28
        versionCode 1
        versionName "1.0"
        //testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }

    // Add for grpc
    lintOptions {
        disable 'GoogleAppIndexingWarning', 'HardcodedText', 'InvalidPackage'
        textReport true
        textOutput "stdout"
    }
}

// Add for grpc
protobuf {
    protoc { artifact = 'com.google.protobuf:protoc:3.5.1-1' }
    plugins {
        javalite { artifact = "com.google.protobuf:protoc-gen-javalite:3.0.0" }
        grpc { artifact = 'io.grpc:protoc-gen-grpc-java:1.14.0' }
    }
    generateProtoTasks {
        all().each { task ->
            task.plugins {
                javalite {}
                grpc {
                    // Options added to --grpc_out
                    option 'lite'
                }
            }
        }
    }
}

//kotlin {
//    experimental {
//        coroutines "enable"
//    }
//}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation"org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    //implementation 'com.android.support:appcompat-v7:28.0.0'
    //implementation 'com.android.support.constraint:constraint-layout:1.1.3'

    // Add for androidx
    implementation 'androidx.appcompat:appcompat:1.1.0-alpha01'
    implementation 'androidx.constraintlayout:constraintlayout:2.0.0-alpha3'
    implementation "androidx.lifecycle:lifecycle-extensions:2.0.0"

    // Add for grpc
    implementation 'io.grpc:grpc-okhttp:1.14.0'
    implementation 'io.grpc:grpc-protobuf-lite:1.14.0'
    implementation 'io.grpc:grpc-stub:1.14.0'
    implementation 'javax.annotation:javax.annotation-api:1.2'

    // Add or coroutines
    //implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.2.2'
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.2.2'

    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.2'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
}

```

นำไฟล์ helloworld.proto ไปไว้ภายใต้ main/proto/helloworld.proto ดังภาพ

![project-struct](https://gitlab.com/sonarinno/android-grpc/raw/develop/readme-image/project-struct.jpg)

ทำการ Sync โปรเจค
## การ Build protobuf ด้วย Android Project
ไปที่ Build -> Make Project  
>เข้าไปดูผลการ Build ที่ android-grpc/app/build/generated/source/proto/debug/grpc/io/grpc/examples/helloworld

![grpc-output-struct](https://gitlab.com/sonarinno/android-grpc/raw/develop/readme-image/grpc-output-struct.jpg)

>จะได้ class ชื่อ GreeterGrpc.java ซึ่งมาจากชื่อ service Greeter และชื่อ package io.grpc.examples.helloworld ก็ได้มาจาก option java_package = "io.grpc.examples.helloworld";

## การใช้งาน Class หลังการ Build protobuf
ทำการสร้างและแก้ไขไฟล์ต่างๆ ดังนี้

![android-output-struct](https://gitlab.com/sonarinno/android-grpc/raw/develop/readme-image/android-project-struct.jpg)

สร้าง class GrpcService สำหรับสร้าง channel เพื่อเชื่อมต่อกับ gRPC server

```java
class GrpcService(private val host: String, private val port: Int) {

    fun createManagedChannel() = ManagedChannelBuilder.forAddress(host, port)
        .executor(Executors.newSingleThreadExecutor())
        .usePlaintext()
        .build()

}
```

สรา้ง class GreeterRemoteDataSource เพื่อเชื่อมต่อและส่งข้อมูลไปให้ gRPC server

```java
class GreeterRemoteDataSource constructor(private val grpcService: GrpcService) {
    suspend fun sayHello(message: String) = withContext(Dispatchers.IO) {

        return@withContext try {

            val channel = grpcService.createManagedChannel()

            val stub = GreeterGrpc.newBlockingStub(channel)

            val request = HelloRequest.newBuilder().setName(message).build()

            val reply = stub.sayHello(request)

            channel?.shutdown()?.awaitTermination(1, TimeUnit.SECONDS)

            reply.message
        } catch (e: Exception) {
            String.format("Failed... : %s", e.message)
        }
    }

}
```
### Mapping Code
การเรียกใช้ code kotlin บรรทัดนี้
```java
val stub = GreeterGrpc.newBlockingStub(channel)
```
คือ protobuff บรรทัดนี้
```protobuf
// The greeting service definition.
service Greeter {
    //  ...
}
```
การเรียกใช้ code kotlin บรรทัดนี้
```java
val request = HelloRequest.newBuilder().setName(message).build()
```
คือ protobuff บรรทัดนี้
```protobuf
// The request message containing the user's name
message HelloRequest {
    string name = 1;
}
```
การเรียกใช้ code kotlin บรรทัดนี้
```java
val reply = stub.sayHello(request)
```
คือ protobuff บรรทัดนี้
```protobuf
// Sends a greeting
rpc SayHello(HelloRequest) returns (HelloResponse) {}
```

สร้าง class GreeterRepository สำหรับเรียกใช้งาน DataSource

```java
class GreeterRepository constructor(private val greeterRemoteDataSource: GreeterRemoteDataSource) {

    suspend fun sayHello(message: String): String {

        return greeterRemoteDataSource.sayHello(message)
    }
}
```

สร้าง sealed class Result สำหรับใช้กำหนด Specification ของ Class อื่น
```java
/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out R> {

    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()
    object Loading : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
            Loading -> "Loading"
        }
    }
}

/**
 * `true` if [Result] is of type [Success] & holds non-null [Success.data].
 */
val Result<*>.succeeded
    get() = this is Result.Success && data != null
```

สร้าง abstract class UseCase สำหรับใช้กำหนด Specification ของ Class อื่น
```java
/**
 * Executes business logic synchronously or asynchronously using a [Coroutines].
 */
abstract class UseCase<in Params, R> {

    /** Executes the use case asynchronously and places the [Results] in a MutableLiveData
     *
     * @param parameters the input parameters to run the use case with
     * @param result the MutableLiveData where the result is posted to
     *
     */
    suspend operator fun invoke(parameters: Params, result: MutableLiveData<Result<R>>) {
        try {
            withContext(Dispatchers.Default) {
                result.postValue(Result.Loading)
                try {
                    execute(parameters).let { useCaseResult ->
                        result.postValue(Result.Success(useCaseResult))
                    }
                } catch (e: Exception) {
                    result.postValue(Result.Error(e))
                }
            }
        } catch (e: Exception) {
            result.postValue(Result.Error(e))
        }
    }

    /** Executes the use case asynchronously and returns a [Results] in a new LiveData object.
     *
     * @return an observable [LiveData] with a [Results].
     *
     * @param parameters the input parameters to run the use case with
     */
    suspend operator fun invoke(parameters: Params): LiveData<Result<R>> {
        val liveCallback: MutableLiveData<Result<R>> = MutableLiveData()
        this(parameters, liveCallback)
        return liveCallback
    }

    /**
     * Override this to set the code to be executed.
     */
    @Throws(RuntimeException::class)
    protected abstract suspend fun execute(parameters: Params): R
}

suspend operator fun <R> UseCase<Unit, R>.invoke(): LiveData<Result<R>> = this(Unit)
suspend operator fun <R> UseCase<Unit, R>.invoke(result: MutableLiveData<Result<R>>) = this(Unit, result)
```

สร้าง class SayHelloUseCase สำหรับกรณีที่ส่งค่าไปให้ gRPC server โดยส่งค่าผ่าน function sayHello

```java
class SayHelloUseCase constructor(
    private val greeterRepository: GreeterRepository
): UseCase<String, String>() {

    override suspend fun execute(parameters: String): String {

        return greeterRepository.sayHello(parameters)
    }

}
```

สร้าง class GreeterViewModel เพื่อใช้ในการ control พวก View

```java
class GreeterViewModel constructor(private val sayHelloUseCase: SayHelloUseCase) : ViewModel() {

    private var sayHelloJob: Job? = null

    fun sayHello(message: String): LiveData<Result<String>> {

        val liveDataMerger = MediatorLiveData<Result<String>>()

        sayHelloJob?.cancel()

        sayHelloJob = GlobalScope.launch(Dispatchers.Main) {

            liveDataMerger.addSource(sayHelloUseCase.invoke(message)) { value ->
                liveDataMerger.setValue(value)
            }

        }

        return liveDataMerger
    }

    override fun onCleared() {
        super.onCleared()
        sayHelloJob?.cancel()
    }

}
```

สร้าง class GreeterViewModelFactory เพื่อสร้าง GreeterViewModel ให้พร้อมนำไปใช้งานใน Activity หรือ Fragment

```java
class GreeterViewModelFactory(
    private val sayHelloUseCase: SayHelloUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GreeterViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return GreeterViewModel(sayHelloUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
```

สร้าง object Injector สำหรับ Provide instance ให้กับ class อื่น ๆ ภายใน greeter

```java
object Injector {

    // Provide GrpcService
    fun provideGrpcService() = GrpcService(
        "35.240.235.132",
        50051
    )

    // Provide DataSource
    fun provideGreeterRemoteDataSource(grpcService: GrpcService) = GreeterRemoteDataSource(grpcService)

    // Provide Repository
    fun provideGreeterRepository(greeterRemoteDataSource: GreeterRemoteDataSource) = GreeterRepository(
        greeterRemoteDataSource
    )

    // Provide UseCase
    fun provideSayHelloUseCase(greeterRepository: GreeterRepository) = SayHelloUseCase(
        greeterRepository
    )

    // Provide ViewModelFactory
    fun provideGreeterViewModelFactory() = GreeterViewModelFactory(
        provideSayHelloUseCase(
            provideGreeterRepository(
                provideGreeterRemoteDataSource(
                    provideGrpcService()
                )
            )
        )
    )
}
```

เรียกใช้ GreeterViewModel ใน Activity

```java
class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: GreeterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Injector.provideGreeterViewModelFactory().let { factory ->
            viewModel = ViewModelProviders.of(this, factory).get(GreeterViewModel::class.java)
        }

        btnSend.setOnClickListener {

            onSend(etMessage.text.toString())

        }

    }

    private fun onSend(message: String) {

        etMessage.setText("")

        viewModel.sayHello(message).observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    tvResult.text = ("Sending...")
                }
                is Result.Success -> {
                    tvResult.text = it.data
                }
                is Result.Error -> {
                    tvResult.text = ("Not receive")
                }
            }
        })

    }
}
```

ปรับปรุงไฟล์ activity_main.xml ให้เหมาะสมกับ MainActivity.kt

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
            android:id="@+id/tvResult"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="Response"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent"/>

    <androidx.appcompat.widget.AppCompatEditText
            android:id="@+id/etMessage"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:inputType="textPersonName"
            android:text="gRPC"
            android:ems="10"
            android:layout_marginEnd="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            android:layout_marginRight="8dp"
            android:layout_marginStart="8dp"
            app:layout_constraintStart_toStartOf="parent"
            android:layout_marginLeft="8dp"
            android:layout_marginTop="8dp"
            app:layout_constraintTop_toTopOf="parent"/>

    <androidx.appcompat.widget.AppCompatButton
            android:text="Send"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/btnSend"
            app:layout_constraintStart_toStartOf="parent" android:layout_marginLeft="8dp"
            android:layout_marginStart="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            android:layout_marginEnd="8dp"
            android:layout_marginRight="8dp"
            app:layout_constraintTop_toBottomOf="@+id/tvResult"
            android:layout_marginBottom="8dp"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintVertical_bias="0.91"/>

</androidx.constraintlayout.widget.ConstraintLayout>
```

## ทดสอบการทำงาน
* ทำการ run Android Application
* ใส่ข้อความใน EditText ว่า “gRPC” ถ้าได้ข้อความตอบกลับมาตามด้านล่างนี้ก็แสดงว่าเราได้ส่งข้อมูลไปให้ gRPC server สำเร็จ
>Hello gRPC